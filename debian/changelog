python-dropbox (12.0.2-2) unstable; urgency=medium

  * Team upload.
  * Patch-out dependency on python3-six

 -- Alexandre Detiste <tchet@debian.org>  Fri, 10 Jan 2025 00:44:59 +0100

python-dropbox (12.0.2-1) unstable; urgency=low

  [ Alexandre Detiste ]
  * New upstream version 12.0.2

  [ Michael Fladischer ]
  * Update year in d/copyright.

 -- Michael Fladischer <fladi@debian.org>  Thu, 18 Jul 2024 20:44:06 +0000

python-dropbox (12.0.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.7.0 (routine-update)
  * Build-Depends: s/dh-python/dh-sequence-python3/ (routine-update)
  * Refresh patch.

 -- Alexandre Detiste <tchet@debian.org>  Wed, 22 May 2024 17:52:01 +0200

python-dropbox (11.36.2-1) unstable; urgency=medium

  * New upstream version 11.36.2
  * Bump Standards-Version to 4.6.2.
  * Update d/copyright with new years.
  * Build using pybuild-plugin-pyproject.
  * Refresh patches.

 -- Michael Fladischer <fladi@debian.org>  Mon, 19 Jun 2023 20:18:54 +0000

python-dropbox (11.34.0-1) unstable; urgency=low

  * New upstream release.
  * Use github tags instead of releases for d/watch.
  * Refresh patches.
  * Bump Standards-Version to 4.6.1.0.

 -- Michael Fladischer <fladi@debian.org>  Tue, 13 Sep 2022 20:08:44 +0000

python-dropbox (11.30.0-1) unstable; urgency=low

  * New upstream release.

 -- Michael Fladischer <fladi@debian.org>  Wed, 27 Apr 2022 13:08:50 +0000

python-dropbox (11.26.0-1) unstable; urgency=low

  * New upstream release.

 -- Michael Fladischer <fladi@debian.org>  Mon, 31 Jan 2022 14:08:59 +0000

python-dropbox (11.25.0-1) unstable; urgency=low

  * New upstream release.
  * Update year in d/copyright.

 -- Michael Fladischer <fladi@debian.org>  Thu, 06 Jan 2022 21:47:46 +0000

python-dropbox (11.21.0-1) unstable; urgency=low

  * New upstream release.
  * Update d/watch to work with github again.
  * Bump Standards-Version to 4.6.0.1.
  * Add python3-stone to Build-Depends.
  * Clean up .eggs/README.txt to allow two builds in a row.
  * Add patch to remove versioned requirement on pytest-runner.

 -- Michael Fladischer <fladi@debian.org>  Sat, 06 Nov 2021 18:00:49 +0000

python-dropbox (10.10.0-1) unstable; urgency=low

  * New upstream release.
  * Bump Standards-Version to 4.5.1.
  * Use uscan version 4.

 -- Michael Fladischer <fladi@debian.org>  Mon, 30 Nov 2020 21:21:00 +0100

python-dropbox (10.6.0-1) unstable; urgency=low

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Michael Fladischer ]
  * New upstream release.

 -- Michael Fladischer <fladi@debian.org>  Thu, 15 Oct 2020 21:16:21 +0200

python-dropbox (10.2.0-1) unstable; urgency=low

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Michael Fladischer ]
  * New upstream release.
  * Bump debhelper version to 13.
  * Bump Standards-Version to 4.5.0.

 -- Michael Fladischer <fladi@debian.org>  Mon, 15 Jun 2020 21:47:06 +0200

python-dropbox (9.4.0-2) unstable; urgency=low

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.

  [ Michael Fladischer ]
  * Set Rules-Requires-Root: no.

 -- Michael Fladischer <fladi@debian.org>  Wed, 11 Dec 2019 17:28:01 +0100

python-dropbox (9.4.0-1) unstable; urgency=low

  * New upstream release.
  * Bump debhelper compatibility and version to 12 and switch to
    debhelper-compat.
  * Bump Standards-Version to 4.4.0.

 -- Michael Fladischer <fladi@debian.org>  Wed, 17 Jul 2019 16:52:02 +0200

python-dropbox (9.3.0-1) unstable; urgency=low

  * New upstream release.

 -- Michael Fladischer <fladi@debian.org>  Mon, 17 Dec 2018 15:22:18 +0100

python-dropbox (9.2.0-1) unstable; urgency=low

  * New upstream release.

 -- Michael Fladischer <fladi@debian.org>  Wed, 28 Nov 2018 19:02:27 +0100

python-dropbox (9.1.0-1) unstable; urgency=low

  * New upstream release.

 -- Michael Fladischer <fladi@debian.org>  Mon, 29 Oct 2018 18:49:02 +0100

python-dropbox (9.0.0-1) unstable; urgency=low

  [ Ondřej Nový ]
  * d/control: Remove ancient X-Python3-Version field

  [ Michael Fladischer ]
  * New upstream release (Closes: #903525).
  * Set d/watch to use github release tarballs.
  * Clean up files in dropbox.egg-info/ to allow two builds in a row.
  * Bump Standards-Version to 4.2.1.

 -- Michael Fladischer <fladi@debian.org>  Mon, 27 Aug 2018 19:25:58 +0200

python-dropbox (8.7.1-1) unstable; urgency=low

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org

  [ Michael Fladischer ]
  * New upstream release.

 -- Michael Fladischer <fladi@debian.org>  Fri, 16 Feb 2018 10:02:49 +0100

python-dropbox (8.7.0-1) unstable; urgency=low

  * New upstream release.

 -- Michael Fladischer <fladi@debian.org>  Sun, 04 Feb 2018 15:12:49 +0100

python-dropbox (8.6.0-1) unstable; urgency=low

  * New upstream release.
  * Enable autopkgtest-pkg-python testsuite.

 -- Michael Fladischer <fladi@debian.org>  Sat, 27 Jan 2018 13:44:46 +0100

python-dropbox (8.5.1-1) unstable; urgency=low

  * New upstream release.
  * Always use pristine-tar.
  * Bump debhelper compatibility and version to 11.
  * Bump Standards-Version to 4.1.3.
  * Run wrap-and-sort -bast to reduce diff size of future changes.

 -- Michael Fladischer <fladi@debian.org>  Mon, 08 Jan 2018 18:59:20 +0100

python-dropbox (8.5.0-1) unstable; urgency=low

  * New upstream release.

 -- Michael Fladischer <fladi@debian.org>  Thu, 16 Nov 2017 10:36:36 +0100

python-dropbox (8.4.1-1) unstable; urgency=low

  * New upstream release.
  * Clean up dropbox.egg-info/PKG-INFO to allow two builds in a row.

 -- Michael Fladischer <fladi@debian.org>  Wed, 08 Nov 2017 09:39:07 +0100

python-dropbox (8.4.0-1) unstable; urgency=low

  * New upstream release.
  * Add missing debian/gbp.conf.
  * Update d/watch to use pypi simple API.
  * Clean up modified files to allow two builds in a row.
  * Bump Standards-Version to 4.1.1.

 -- Michael Fladischer <fladi@debian.org>  Tue, 17 Oct 2017 20:17:19 +0200

python-dropbox (8.0.0-1) unstable; urgency=low

  * Initial release (Closes: #828940).

 -- Michael Fladischer <fladi@debian.org>  Mon, 31 Jul 2017 21:30:40 +0200
